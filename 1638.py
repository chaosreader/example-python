mod = int(1e9)+7
n = int(input())
aa=[]
bb=[x[:] for x in [[0] * n] * n]
for i in range(n):
	aa.append([x for x in input()])

for i in range(n):
	for j in range(n):
		if aa[i][j] == '*':
			bb[i][j] = 0
		elif (i == 0 and j == 0):
			bb[0][0] = 1
		elif (i > 0 and j == 0):
			bb[i][j] = bb[i-1][j]
		elif (j > 0 and i == 0):
			bb[i][j] = bb[i][j-1]
		else:	
			bb[i][j] = (bb[i][j-1] + bb[i-1][j]) % mod
print(bb[n-1][n-1])
