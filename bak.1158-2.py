n, x = map(int, input().split())
g = list(map(int, input().split()))
r = list(map(int, input().split()))
d = [0] * (x+1)
for h, s in zip(g, r):
	for y in range(x, h-1, -1):
		d[y] = max(d[y], (d[y-h] + s))
print(d[x])
