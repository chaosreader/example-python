#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

const int N = 500000;

vector<int> aa[N];
int dp0[N];
int dp1[N];

void dfs(int p, int i)
{
	int x0 = 0;
	int x1 = 1;
	auto &adj = aa[i];
	for (int j : adj)
	{
		if (j != p)
		{
			dfs(i, j);
			x0 += dp1[j];
			x1 += dp0[j];
		}
	}
	dp1[i] = x1;
	dp0[i] = min(x0, x1);
}

int main()
{
	int n, a, b;
	cin >> n;
	for (int i = 0; i < n-1; i++)
	{
		cin >> a >> b;
		a--;
		b--;
		aa[a].push_back(b);
		aa[b].push_back(a);
	}
	dfs(-1, 0);
	cout << dp0[0] << endl;
	return 0;
}
