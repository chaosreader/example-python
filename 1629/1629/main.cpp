#include <iostream>
#include <algorithm>
#include <utility>
#include <vector>

using namespace std;

int main()
{
	int n;
	cin >> n;
	vector<pair<int, int>> aa(n);

	for (int i = 0; i < n; i++)
	{
		cin >> aa[i].second >> aa[i].first;
	}
	sort(aa.begin(), aa.end());
	int cnt = 1, nd = aa[0].first;
	for (int i = 1; i < n; i++)
	{
		if (aa[i].second >= nd)
		{
			cnt++;
			nd = aa[i].first;
		}
	}
	cout << cnt << endl;
	return 0;
}