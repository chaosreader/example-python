import sys

sys.setrecursionlimit(100000)

def dfs(p, i):
    x0 = 0
    x1 = 1
    for j in aa[i]:
        if j != p:
            dfs(i, j)
            x0 += dp[1][j]
            x1 += dp[0][j]
    dp[1][i] = x1
    dp[0][i] = min(x0, x1)

n = int(input())
aa = [[] for _ in range(n)]
dp = [[0 for i in range(n)] for j in range(2)]
for i in range(n-1):
    a, b = map(int, input().split())
    a -= 1
    b -= 1
    aa[a].append(b)
    aa[b].append(a)
dfs(-1, 0)
print(dp[0][0])
