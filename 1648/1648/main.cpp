#include <iostream>
#include <vector>

using namespace std;

int n;
vector<long long> tree;
vector<long long> aa;

long long sum(long long k) {
	long long s = 0;
	while (k >= 1) 
	{
		s += tree[k];
		k -= k & -k;
	}
	return s;
}

long long sum(long long l, long long r) {
	return sum(r) - sum(l - 1);
}

void add(int k, long long x) {
	while (k <= n) 
	{
		tree[k] += x;
		k += k & -k;
	}
}

int main()
{
	long long q, t, a, b;
	cin >> n >> q;
	aa = vector<long long>(n);
	tree = vector<long long>(n+1);
	for (int i = 0; i < n; i++)
	{
		cin >> aa[i];
		add(i+1, aa[i]);
	}
	while (q--)
	{
		cin >> t >> a >> b;
		if (t==1)
		{
			add(a, b - aa[a-1]);
			aa[a-1] = b;
		}
		else
		{
			cout << sum(a, b) << endl;
		}
	}
	return 0;
}
