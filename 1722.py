ll = [(0,0), (0,1),(1, 0), (1,1)]

def twoXtwo(aa, bb):
	return([[aa[0][0]*bb[0][0]+aa[0][1]*bb[1][0],
	aa[0][0]*bb[0][1]+aa[0][1]*bb[1][1]],
	[aa[1][0]*bb[0][0]+aa[1][1]*bb[1][0],
	aa[1][0]*bb[0][1]+aa[1][1]*bb[1][1]]])

def power(aa, b):
	ans = [[1,0],[0,1]]
	while b > 0:
		if b%2:
			ans = twoXtwo(ans, aa) 
			for i, j in ll:
				ans[i][j] %= MOD
		b = b >> 1
		aa = twoXtwo(aa, aa) 
		for i,j in ll:
			aa[i][j] %= MOD
	return(ans)

MOD = int(1e9)+7
n = int(input())
aa = [[0,1],[1,1]]
print(power(aa, n)[0][1])
