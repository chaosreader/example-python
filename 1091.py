import math

def tree(n, aa):
	n = 2**math.ceil(math.log(n, 2))
	for i in range(2*n - len(aa)):
		aa.append(math.inf)
	for i in range(n, 2*n):
		aa[i] = aa[i-n]
	for i in range(n-1, 0, -1):
		aa[i] = min(aa[2*i], aa[2*i+1])
	return aa, n

def update(x, i, n, arr):
    arr[i] = x
    i //= 2
    while i >= 1:
        arr[i] = min(arr[2 * i], arr[2 * i + 1])
        i //= 2
    return arr

def query(x, n, arr):
    i = 1
    while i < n:
        if arr[2 * i + 1] <= x:
            i = 2 * i + 1
        elif arr[2 * i] <= x:
            i = 2 * i
        else:
            return 0
    return i


n, m = map(int, input().split())
aa = list(sorted(map(int, input().split())))
aa, n = tree(n, aa)
bb = list(map(int, input().split()))
for i in range(m):
    res = query(bb[i], n, aa)
    if res and aa[res] != math.inf:
        print(aa[res])
        aa = update(math.inf, res, n, aa)
    else:
        print(-1)

