def getsum(BITTree,i):
    s = 0 #initialize result
    i = i+1
    while i > 0:
        s += BITTree[i]
        i -= i & (-i)
    return s

def updatebit(BITTree , n , i ,v):
    i += 1
    while i <= n:
        BITTree[i] += v
        i += i & (-i)

def construct(arr, n):
    BITTree = [0]*(n+1)
    for i in range(n):
        updatebit(BITTree, n, i, arr[i])
    return BITTree

def rangesum(BITTree, l, r):
    return (getsum(BITTree, r)-getsum(BITTree, (l)))

n, q = map(int, input().split())
aa = list(map(int, input().split()))
BITTree = construct(aa,len(aa))
for i in range(q):
    t, a, b = map(int, input().split())
    if t == 1:
        updatebit(BITTree, len(aa), a, b)
    else:
        print(rangesum(BITTree, a, b))
