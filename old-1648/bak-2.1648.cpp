// C++ code to demonstrate operations of Binary Index Tree 
#include <iostream> 

using namespace std;

/*         n --> No. of elements present in input array.
BITree[0..n] --> Array that represents Binary Indexed Tree.
arr[0..n-1] --> Input array for which prefix sum is evaluated. */

// Returns sum of arr[0..index]. This function assumes 
// that the array is preprocessed and partial sums of 
// array elements are stored in BITree[]. 
int getSum(long BITree[], int index)
{
	int sum = 0; // Iniialize result 

				 // index in BITree[] is 1 more than the index in arr[] 
	index = index + 1;

	// Traverse ancestors of BITree[index] 
	while (index>0)
	{
		// Add current element of BITree to sum 
		sum += BITree[index];

		// Move index to parent node in getSum View 
		index -= index & (-index);
	}
	return sum;
}

// Updates a node in Binary Index Tree (BITree) at given index 
// in BITree. The given value 'val' is added to BITree[i] and  
// all of its ancestors in tree. 
void updateBIT(long BITree[], long n, int index, long val)
{
	// index in BITree[] is 1 more than the index in arr[] 
	index = index + 1;

	// Traverse all ancestors and add 'val' 
	while (index <= n)
	{
		// Add 'val' to current node of BI Tree 
		BITree[index] += val;

		// Update index to that of parent in update View 
		index += index & (-index);
	}
}

// Constructs and returns a Binary Indexed Tree for given 
// array of size n. 
long *constructBITree(long arr[], long n)
{
	// Create and initialize BITree[] as 0 
	long *BITree = new long[n + 1];
	for (int i = 1; i <= n; i++)
		BITree[i] = 0;

	// Store the actual values in BITree[] using update() 
	for (int i = 0; i<n; i++)
		updateBIT(BITree, n, i, arr[i]);

	return BITree;
}


// Driver program to test above functions 
int main()
{
	long n, q, u;
	int t, k, a, b;
	cin >> n >> q;

	long *freq = new long[n];
	for (int i = 0; i < n; i++)
	{
		cin >> freq[i];
	}
	long *BITree = constructBITree(freq, n);
	while (q--)
	{
		cin >> t;
		if (t==1)
		{
			cin >> k >> u;
			cout << freq[k-1] << endl;
			cout << u - freq[k-1] << endl;
			updateBIT(BITree, n, k, u - freq[k-1]);
		}
		else
		{
			cin >> a >> b;
			if (a > 1)
			{
				cout << getSum(BITree, b - 1) - getSum(BITree, a - 2) << endl;
			}
			else
			{
				cout << getSum(BITree, b - 1) << endl;;
			}
		}
	}
	for (int i = 1; i <= n; i++)
		cout << BITree[i] << " ";
	return 0;
}