from math import inf, log, ceil
 
 
def build(n, arr):
    n = 2 ** ceil(log(n) / log(2))
    arr += [inf for i in range(2 * n - len(arr))]
 
    for i in range(n, n * 2):
        arr[i] = arr[i - n]
 
    for i in range(n - 1, 0, -1):
        arr[i] = min(arr[2 * i], arr[2 * i + 1])
    return arr, n
 
 
def update(x, i, n, arr):
    arr[i] = x
    i //= 2
    while i >= 1:
        arr[i] = min(arr[2 * i], arr[2 * i + 1])
        i //= 2
    return arr
 
 
def query(x, n, arr):
    i = 1
    while i < n:
        if arr[2 * i + 1] <= x:
            i = 2 * i + 1
        elif arr[2 * i] <= x:
            i = 2 * i
        else:
            return 0
    return i
 
 
n, m = map(int, input().split())
arr = list(sorted(map(int, input().split())))
arr, n = build(n, arr)
customers = list(map(int, input().split()))
for i in range(m):
    res = query(customers[i], n, arr)
    if res and arr[res] != inf:
        print(arr[res])
        arr = update(inf, res, n, arr)
    else:
        print(-1)