#include <iostream>

using namespace std;

long* tree;

long query(int i)
{
	long x = 0;
	while (i >= 0)
	{
		x += tree[i];
		i = i & (i + 1);
		i--;
	}
	return x;
}

void update(int i, int n, int x)
{
	while (i < n)
	{
		cout << i << ' ' << tree[i] << endl;
		tree[i] += x;
		i = (i | (i + 1));
	}
}

int main()
{
	long n, q, u;
	int t, k, a, b;
	cin >> n >> q;
	int* aa = new int[n];
	tree = new long[n];
	for (int i = 0; i < 0; i++)
	{
		tree[i] = 0;
		cout << tree[i] << ' ';
	}
	cout << endl;
	for (int i = 0; i < n; i++)
	{
		cin >> aa[i];
		update(i, n, aa[i]);
	}
	
	while (q--)
		{
			cin >> t;
			if (t==1)
			{
				cin >> k >> u;
				k--;
				update(k, n, u - aa[k]);
				aa[k] = u;
			}
			else
			{
				cin >> a >> b;
				a--;
				b--;
				cout << query(b) - query(a - 1) << endl;
			}
		}
		
	return 0;
}