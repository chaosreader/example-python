a = [0] * (int(input())+1)
for i in range(1, len(a)):
	x = int(1e6)+1
	bb = [int(y) for y in str(i)]
	for j in bb:
		if j:
			x = min(x, (a[i-j]+1))
	a[i] = x
print(a[len(a)-1])
