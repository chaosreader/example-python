#include <iostream>
#include <climits>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, x;
	cin >> n >> x;
	vector<int> aa(x + 1, 1000001);
	aa[0] = 0;
	vector<int> cc(n);
	for (int i = 0; i < n; i++)
	{
		cin >> cc[i];
	}
	for (int i = 1; i < x+1; i++)
	{
		for (int j = 0; j < n; j++)
		{
			if (i - cc[j] >= 0)
			{
				aa[i] = min(aa[i], aa[(i - cc[j])] + 1);
			}
		}
	}
	cout << ((aa[x] == 1000001) ? -1 : aa[x]) << endl;
}

/*
for (int i = 0; i < n; i++)
{
cout << cc[i] << " ";
}
cout << endl;
for (int i = 0; i < x+1; i++)
{
cout << aa[i] << " ";
}
cout << endl;
*/
