aa = [0]*int(1e6+1)
mod = int(1e9+7)
aa[0] = 1
n = int(input())
for i in range(1,n+1):
	if i <= 6:
		aa[i] = sum(aa[:i])
	else:
		aa[i] = sum(aa[i-6:i]) % mod
print(aa[n])
