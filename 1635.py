mod = int(1e9)+7
n, x = map(int,input().split())
c = sorted(map(int, input().split()))
aa = [0]*(x+1)
aa[0] = 1
for i in range(1, x+1):
	for j in c:
		if j <= i:
			aa[i] += aa[i-j]
			aa[i] %= mod
		else:
			break
print(aa[x])
			
