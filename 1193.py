from collections import deque
n, m = map(int, input().split())
cc = [list(input()) for _ in range(n)]
dir = ((1, 0, 'R'), (0, 1, 'D'), (-1, 0, 'L'), (0, -1, 'U'))
for Y in range(n):
    for X in range(m):
        if cc[Y][X] != 'A':
            continue
        Q = deque([(Y,X)])
        while Q:
            y, x = Q.popleft()
            if cc[y][x] == 'B':
#                Q.clear()
                break
            for i, j, c in dir:
                A = x+i
                B = y+j
                if 0 <= A and A < m and 0 <= B and B < n:
                    R = cc[B][A]
                    if R == '.':
                        cc[B][A] = (x, y, c)
                        Q.append((B, A))
                    elif R == 'B':
                        print('YES')
                        p = [c]
                        while cc[y][x] != 'A':
                            x, y, c = cc[y][x]
                            p.append(c)
                        print(len(p))
                        print(''.join(p[::-1]))
                        raise SystemExit
        print('NO')
