MOD = int(1e9)+7
n = int(input())
for _ in range(n):
	a, b = map(int, input().split())
	ans = 1
	while b > 0:
		if b%2:
			ans *= a 
			ans %= MOD
		b = b >> 1
		a *= a 
		a %= MOD
	print(ans)
