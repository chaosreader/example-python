class Obj:
	def __init__(self):
		self.maxi = 0
		self.uni = 0
 
def findDia(adj, i, par):
	s = [[1, True, 0]]
	val = {}
	while s:
		t = s[-1]
		if t[1]:
			i = t[0]
			s[-1][1] = False
			for jk in adj[i]:
				if jk != t[2]:
					s.append([jk, True, i])
		else:
			maxi = 0
			uni1 = 0
			uni = 0
			i = t[0]
			par = t[2]
			for j in adj[i]:
				if j != par:
					tmp = val[j]
					maxi = max(tmp.maxi, maxi)
					if tmp.uni > uni:
						uni1 = uni
						uni = tmp.uni
					elif tmp.uni > uni1:
						uni1 = tmp.uni
			finMaxi = max(maxi, uni + uni1 + 1)
			finObj = Obj()
			finObj.maxi = finMaxi
			finObj.uni = uni + 1
			val[i] = finObj
			s.pop()
	return val[1]
 
n = int(input())
adj = [[] for i in range(n+1)]
adj[1].append(0)
for i in range(n-1):
    a, b = map(int, input().split())
    adj[a].append(b)
    adj[b].append(a)
print(0 if (adj == [[], [0]]) else (findDia(adj, 1, 0).maxi-1))
