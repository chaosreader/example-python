n, x = map(int, input().split())
aa = sorted(list(map(int, input().split())))

ans = n
i = 0
j = n-1
while i < j:
	while i < j and (aa[i] + aa[j] > x):
		j -= 1
	if i < j and (aa[i] + aa[j] <= x):
		ans -= 1
	i += 1
	j -= 1
print(ans)
