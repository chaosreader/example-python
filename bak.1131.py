import sys

sys.setrecursionlimit(100000)

def dfs(p, i, d):
    global d_
    global i_
    if d_ < d:
        d_ = d
        i_ = i
    for j in aa[i]:
        if j != p:
            dfs(i, j, d+1)

n = int(input())
aa = [[] for _ in range(n)]
i_ = None
if n == 1:
    print(0)
    sys.exit()
for i in range(n-1):
    a, b = map(int, input().split())
    a -= 1
    b -= 1
    aa[a].append(b)
    aa[b].append(a)
d_ = 0
dfs(-1, 0, 0)
d_ = 0
dfs(-1, i_, 0)
print(d_)
