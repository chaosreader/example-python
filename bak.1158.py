n, x = map(int, input().split())
hh = list(map(int, input().split()))
ss = list(map(int, input().split()))
#hh = [int(x) for x in input().split()]
#ss = [int(x) for x in input().split()]

dp = [0] * (x+1)
for i in range(n):
	h = hh[i]
	s = ss[i]
	y = x
	while y >= h:
		dp[y] = max(dp[y], (dp[y-h] + s))
		y -= 1
print(dp[x])
