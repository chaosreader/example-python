t = int(input())
while t:
    t -= 1
    aa = input().split()
    y = int(aa[0])
    x = int(aa[1])
    layer = max(x, y)
    ans = layer*layer
    if layer % 2:
        ans = ans - abs(x-layer) - abs(y-1)
    else:
        ans = ans - abs(x-1) - abs(y-layer)
    print(ans)
