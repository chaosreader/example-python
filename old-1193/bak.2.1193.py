from collections import deque

class Point:
    def __init__(self,x: int, y: int):
        self.x = x
        self.y = y

class queueNode:
    def __init__(self,pt: Point, dist: int):
        self.pt = pt  # The cordinates of the cell
        self.dist = dist  # Cell's distance from the source

def isValid(row: int, col: int):
    return (row >= 0) and (row < ROW) and (col >= 0) and (col < COL)

rowNum = [-1, 0, 0, 1]
colNum = [0, -1, 1, 0]
sDir = ['D', 'R', 'L', 'U']
#sDir = ['U', 'L', 'R', 'D']

def BFS(mat, src: Point, dest: Point):
    if mat[src.x][src.y]!='.' or mat[dest.x][dest.y]!='.':
        return -1

    visited = [[False for i in range(COL)] for j in range(ROW)]

    visited[src.x][src.y] = True
    q = deque()
    s = queueNode(src,0)
    q.append(s) #  Enqueue source cell
    while q:
        curr = q.popleft() # Dequeue the front cell
        # If we have reached the destination cell,
        # we are done
        pt = curr.pt
        if pt.x == dest.x and pt.y == dest.y:
            return curr.dist
        # Otherwise enqueue its adjacent cells
        for i in range(4):
            row = pt.x + rowNum[i]
            col = pt.y + colNum[i]
            # if adjacent cell is valid, has path
            # and not visited yet, enqueue it.
            if (isValid(row,col) and mat[row][col] == '.' and not visited[row][col]):
                visited[row][col] = True
                Adjcell = queueNode(Point(row,col),curr.dist+1)
                q.append(Adjcell)
                ans[row][col] = min(ans[row][col],curr.dist+1)
    # Return -1 if destination cannot be reached
    return -1

ROW, COL = map(int,input().split())
mat = [list(input().strip()) for _ in range(ROW)]
ans = [[int(1e6)]*COL for _ in range(ROW)]

#mat = [[ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 ],
#           [ 1, 0, 1, 0, 1, 1, 1, 0, 1, 1 ],
#           [ 1, 1, 1, 0, 1, 1, 0, 1, 0, 1 ],
#           [ 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ],
#           [ 1, 1, 1, 0, 1, 1, 1, 0, 1, 0 ],
#           [ 1, 0, 1, 1, 1, 1, 0, 1, 0, 0 ],
#           [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
#           [ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 ],
#           [ 1, 1, 0, 0, 0, 0, 1, 0, 0, 1 ]]
#ROW = 9
#COL = 10

s = ""
tmpi = -1
tmpj = -1
step = 0
source = Point(-1,-1)
dest = Point(-1,-1)
for i in range(ROW):
    for j in range(COL):
        if mat[i][j] == 'A':
            source = Point(i, j)
            ans[i][j] = 0
            mat[i][j] = '.'
#            tmpi = i
#            tmpj = j
        elif mat[i][j] == 'B':
            dest = Point(i, j)
            mat[i][j] = '.'
            tmpi = i
            tmpj = j
#            print(i, j)

dist = BFS(mat,source,dest)

if dist!=-1:
    print("YES")
    print(dist)
#    print(ans)
    while dist:
        step = dist -1
#        print(step)
        for i in range(4):
            ii = tmpi + rowNum[i]
            j = tmpj + colNum[i]
            if isValid(ii, j)and (ans[ii][j] == step):
                tmpi += rowNum[i]
                tmpj += colNum[i]
                s += sDir[i]
                i = 5
#        print(s)
        dist -= 1
    print(s[::-1])
else:
    print("NO")
