from collections import deque

#class Point:
#    def __init__(self,x: int, y: int):
#        self.x = x
#        self.y = y

class queueNode:
    def __init__(self,i: int, j: int, dist: int):
        self.i = i  # The cordinates of the cell
        self.j = j
        self.dist = dist  # Cell's distance from the source

def isValid(row: int, col: int):
    return (row >= 0) and (row < ROW) and (col >= 0) and (col < COL)

rowNum = [-1, 0, 0, 1]
colNum = [0, -1, 1, 0]
sDir = ['D', 'R', 'L', 'U']
#sDir = ['U', 'L', 'R', 'D']

def BFS(mat, srci: int, srcj: int, desti: int, destj: int):
    if mat[srci][srcj]!='.' or mat[desti][destj]!='.':
        return -1

    visited = [[False for i in range(COL)] for j in range(ROW)]

    visited[srci][srcj] = True
    q = deque()
    s = queueNode(srci, srcj, 0)
    q.append(s) #  Enqueue source cell
    while q:
        curr = q.popleft() # Dequeue the front cell
        # If we have reached the destination cell,
        # we are done
        pti = curr.i
        ptj = curr.j
        if pti == desti and ptj == destj:
            return curr.dist
        # Otherwise enqueue its adjacent cells
        for i in range(4):
            row = pti + rowNum[i]
            col = ptj + colNum[i]
            # if adjacent cell is valid, has path
            # and not visited yet, enqueue it.
            if (isValid(row,col) and mat[row][col] == '.' and not visited[row][col]):
                visited[row][col] = True
                Adjcell = queueNode(row,col,curr.dist+1)
                q.append(Adjcell)
                ans[row][col] = min(ans[row][col],curr.dist+1)
    # Return -1 if destination cannot be reached
    return -1

ROW, COL = map(int,input().split())
mat = [list(input().strip()) for _ in range(ROW)]
ans = [[int(1e6)]*COL for _ in range(ROW)]

#mat = [[ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 ],
#           [ 1, 0, 1, 0, 1, 1, 1, 0, 1, 1 ],
#           [ 1, 1, 1, 0, 1, 1, 0, 1, 0, 1 ],
#           [ 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 ],
#           [ 1, 1, 1, 0, 1, 1, 1, 0, 1, 0 ],
#           [ 1, 0, 1, 1, 1, 1, 0, 1, 0, 0 ],
#           [ 1, 0, 0, 0, 0, 0, 0, 0, 0, 1 ],
#           [ 1, 0, 1, 1, 1, 1, 0, 1, 1, 1 ],
#           [ 1, 1, 0, 0, 0, 0, 1, 0, 0, 1 ]]
#ROW = 9
#COL = 10

s = ""
tmpi = -1
tmpj = -1
step = 0
sourcei = -1
sourcej = -1
desti = -1
destj = -1
for i in range(ROW):
    for j in range(COL):
        if mat[i][j] == 'A':
            sourcei = i
            sourcej = j
            ans[i][j] = 0
            mat[i][j] = '.'
#            tmpi = i
#            tmpj = j
        elif mat[i][j] == 'B':
            desti = i
            destj = j
            mat[i][j] = '.'
            tmpi = i
            tmpj = j
#            print(i, j)

dist = BFS(mat,sourcei, sourcej, desti, destj)

if dist!=-1:
    print("YES")
    print(dist)
#    print(ans)
    while dist:
        step = dist -1
#        print(step)
        for i in range(4):
            ii = tmpi + rowNum[i]
            j = tmpj + colNum[i]
            if isValid(ii, j)and (ans[ii][j] == step):
                tmpi += rowNum[i]
                tmpj += colNum[i]
                s += sDir[i]
                i = 5
#        print(s)
        dist -= 1
    print(s[::-1])
else:
    print("NO")
