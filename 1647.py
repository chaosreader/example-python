def query(i):
    x = int(1e9)+1
    while i >= 0:
        x = min(x, Tree[i])
        i &= i+1
        i -= 1
    return x

def update(i, n, x):
    while i < n:
        Tree[i] = min(Tree[i], x)
        i |= i + 1

n, q = map(int, input().split())
aa = list(map(int, input().split()))
ll = []
rr = []
ii = []
Tree = [int(1e9)+1]*n
ans = [0]*q

for i in range(q):
    a, b = map(int, input().split())
    ll.append(a-1)
    rr.append(b-1)
    ii.append(i)
ii = sorted(ii, key=lambda i: ll[i], reverse=True)

h = 0
l = n-1

while l >= 0:
    update(l, n, aa[l])
    while(h < q and ll[ii[h]] == l):
        r = rr[ii[h]]
        tmp = query(r)
        ans[ii[h]] = tmp
        h += 1
    l -= 1

for i in ans:
    print(i)
