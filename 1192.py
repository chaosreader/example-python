def find(i):
    if dsu[i] < 0:
        return i
    else:
        dsu[i] = find(dsu[i])
        return dsu[i]

def join(i, j):
    i = find(i)
    j = find(j)
    if i == j:
        return False
    if dsu[i] > dsu[j]:
        dsu[i] = j
    else:
       if dsu[i] == dsu[j]:
           dsu[i] -= 1
       dsu[j] = i
    return True

n, m = map(int, input().split())
aa = []
dsu = [-1] * (n*m)
k = 0
for _ in range(n):
    tmp = list(input().strip())
    aa.append(tmp)
    for i in tmp:
        if i == '.':
            k += 1
for i in range(n):
    for j in range(1, m):
        if aa[i][j-1] == '.' and aa[i][j] == '.':
            u = i*m+j-1
            v = i*m+j
            if join(u, v):
                k -= 1
for i in range(1, n):
    for j in range(m):
        if aa[i-1][j] == '.' and aa[i][j] == '.':
            u = (i-1)*m+j
            v = i*m+j
            if join(u, v):
                k -= 1
print(k)
