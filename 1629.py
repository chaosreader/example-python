def solve(aa):
	return sorted(aa, key = lambda x: x[1])

n = int(input())
aa = [(0,0)]*n
for i in range(n):
	a, b = map(int, input().split())
	aa[i] = (a,b)
aa = solve(aa)
cnt = 1
nd = aa[0][1]
for i in range(n):
	if aa[i][0] >= nd:
		cnt += 1
		nd = aa[i][1]
print(cnt)
	
