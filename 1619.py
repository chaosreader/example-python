n = int(input())
aa = [0] * n
bb = [0] * n
for i in range(n):
	a, b = map(int,input().split())
	aa[i] = a
	bb[i] = b
aa.sort()
bb.sort()
i = j = ans = cnt = 0
while i < n and j < n:
	if aa[i]< bb[j]:
		cnt += 1
		i += 1
		ans = max(ans, cnt)
	else:
		cnt -= 1
		j += 1
print(ans)
