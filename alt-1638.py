n = int(input())
m = []
for _ in range(n):
  m.append(list(map(lambda x: 1 if x == "." else 0, list(input()))))
  
for i in range(1,n):
  if m[0][i] == 1:
    m[0][i] = m[0][i-1]
  if m[i][0] == 1:
    m[i][0] = m[i-1][0]
 
for r in range(1, n):
  for c in range(1, n):
    if m[r][c] == 1:
      m[r][c] = m[r-1][c] + m[r][c-1]
      m[r][c] %= 1000000007
 
print(m[n-1][n-1])
