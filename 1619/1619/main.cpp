#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
	int n, a, b;
	cin >> n;
	vector<int> aa(2 * n);
	for (int i = 0; i < n; i++)
	{
		cin >> a >> b;
		aa[i * 2 + 0] = a * 2 + 0;
		aa[i * 2 + 1] = b * 2 + 1;
	}
	sort(aa.begin(), aa.end());
	int ans = 0, cnt = 0;
	for (int i = 0; i < 2*n; i++)
	{
		if (aa[i]%2)
		{
			cnt--;
		}
		else
		{
			cnt++;
			ans = max(ans, cnt);
		}
	}
	cout << ans << endl;
	return 0;
}