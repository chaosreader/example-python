n = int(input())
if n % 4 == 1 or n % 4 == 2:
    print("NO")
else:
    print("YES")
    if n % 4 == 0:
        print((n+1)//2)
        i = 1
        while i < (n+1-i):
            print(i, end=" ")
            print(n+1-i, end=" ")
            i += 2
        print()
        print(n//2)
        i = 2
        while i < (n+1-i):
            print(i, end=" ")
            print(n+1-i, end=" ")
            i += 2
        print()
    else:
        print((n+1)//2)
        i = 4
        print("1 2", end=" ")
        while i < (n+4-i):
            print(i, end=" ")
            print(n+4-i, end=" ")
            i += 2
        print()
        print(n//2)
        i = 5
        print("3", end=" ")
        while i < (n+4-i):
            print(i, end=" ")
            print(n+4-i, end=" ")
            i += 2
        print()
