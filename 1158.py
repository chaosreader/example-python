n, x = map(int, input().split())
d = [0] * (x+1)
for h, s in zip(list(map(int, input().split())), list(map(int, input().split()))):
	for y in range(x, h-1, -1):
		d[y] = max(d[y], (d[y-h] + s))
print(d[x])
