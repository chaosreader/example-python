#include <iostream>
#include <vector>

using namespace std;

const int N = 5000000;

vector<int> aa[N];
int d_ = 0, i_;

void dfs(int p, int i, int d)
{
	if (d_ < d)
	{
		d_ = d;
		i_ = i;
	}
	auto &adj = aa[i];
	for (int j : adj)
	{
		if (j != p)
		{
			dfs(i, j, d + 1);
		}
	}
}

int main()
{
	int n, a, b;
	cin >> n;
	for (int i = 0; i < n - 1; i++)
	{
		cin >> a >> b;
		a--;
		b--;
		aa[a].push_back(b);
		aa[b].push_back(a);
	}
	if (n == 1)
	{
		cout << '0' << endl;
		return 0;
	}
	dfs(-1, 0, 0);
	d_ = 0;
	dfs(-1, i_, 0);
	cout << d_ << endl;
	return 0;
}