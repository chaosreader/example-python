def query(i):
    x = 0
    while i:
        x += tree[i-1]
        i &= i-1
    return x

def update(i, n, x):
    while i < n:
        tree[i] += x
        i |= i+1

n, q = map(int, input().split())
tree = [0]*(n)
aa = list(map(int, input().split()))

for i, j in enumerate(aa):
    update(i, n, j)

for _ in range(q):
    t, a, b = map(int, input().split())
    a -= 1
    if t == 1:
        tmp = b - aa[a]
        update(a, n, tmp)
        aa[a] = b
    else:
        print(query(b)-query(a))
