n = int(input())
aa = [0] * (n*2)
for i in range(n):
	a, b = list(map(int,input().split()))
	aa[i*2+0] = a*2+0
	aa[i*2+1] = b*2+1
aa.sort()
ans = 0
cnt = 0
for i in range(n*2):
	if aa[i]%2:
		cnt -= 1
	else:
		cnt += 1
		ans = max(ans, cnt)
print(ans)
