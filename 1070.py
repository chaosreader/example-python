n = int(input())
nn = n//2
rem = n % 2
if n == 2 or n == 3:
    print("NO SOLUTION")
else:
    for i in range(1, nn+1):
        print(2*i, end=" ")
    for i in range(0, nn):
        print(2*i+1, end=" ")
    if rem:
        print(n)
    else:
        print()
